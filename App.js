import React from 'react';
import AppStore from './source/AppStore';

class App extends React.Component {

  render() {
    return (
      <AppStore />
    );
  }
}

export default App;