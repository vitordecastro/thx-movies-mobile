export const getPopularTvShows = (tvShows) => {
  return {
    type: 'GET_POPULAR_TV_SHOWS',
    payload: { tvShows }
  };
};

export const setPage = (page) => {
  return {
    type: 'SET_PAGE',
    payload: { page }
  };
};