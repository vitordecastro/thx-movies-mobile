const INITIAL_STATE = {
  tvShows: [],
  page: 0
};

export default (state = INITIAL_STATE, action) => {

  switch (action.type) {

    case 'GET_POPULAR_TV_SHOWS': {
      const { tvShows } = action.payload;

      return {
        ...state,
        tvShows
      };
    }

    case 'SET_PAGE': {
      const { page } = action.payload;

      return {
        ...state,
        page
      };
    }

    default: {
      return state;
    }

  };

};