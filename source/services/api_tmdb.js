import axios from "axios";

const token = "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJiZmFkNTNhOTM1ZDA3MmEzNDM0ZTg4MjRlNTIyOGE5YiIsInN1YiI6IjVkZjExZGQxYTI4NGViMDAxNzU4MWFiNyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.oQQRl4sr3nc6Zbbszxctlr-QnjizrbITreGYkQxITB4";

const api = axios.create({
  baseURL: 'https://api.themoviedb.org/3/',
  headers: {'Authorization': "bearer " + token }
});

api.interceptors.response.use((response) => response.data);

export default {

  getPopularTvShows: async (params) => api.get('tv/popular', { params }),

}
