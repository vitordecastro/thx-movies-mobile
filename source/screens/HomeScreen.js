import React from 'react';
import { connect } from 'react-redux';

import ListHomeComponent from '../components/home/List';

import api from '../services/api_tmdb';
import {
  getPopularTvShows,
  setPage
} from '../store/actions';

class HomeScreen extends React.Component {

  static navigationOptions = {
    title: 'Home'
  };

  constructor(props) {
    super(props);

    this.getPopularTvShows();
  }

  getPopularTvShows = async () => {

    const { dispatch, page } = this.props;

    let data = await api.getPopularTvShows({ page });

    const tvShows = data.results;
    tvShows.sort((item, item2) => item2.vote_average - item.vote_average);

    dispatch(getPopularTvShows(tvShows));
    dispatch(setPage(data.page + 1));
  }

  render() {

    const { tvShows } = this.props;

    return (
      <ListHomeComponent tvShows={tvShows} getPopularTvShows={this.getPopularTvShows} />
    );
  }
}

const mapStateToProps = (state) => ({
  tvShows: state.tvShows
});

export default connect(mapStateToProps)(HomeScreen);