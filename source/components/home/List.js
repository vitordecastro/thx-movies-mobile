import React from 'react';
import { RefreshControl, FlatList, StyleSheet, View } from 'react-native';

import ListItemHomeComponent from './ListItem';

class ListHomeComponent extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      isFetching: false
    }
  }

  onRefresh = () => {
    this.setState({
      ...this.state,
      isFetching: true
    },
    () => {
      this.props.getPopularTvShows();
      this.setState({
        ...this.state,
        isFetching: false
      });
    });
  }

  render() {

    const { tvShows } = this.props;

    return (
      <FlatList
        data={tvShows}
        keyExtractor={(item) => String(item.id)}
        renderItem={({ item }) => (
          <ListItemHomeComponent item={item} />
        )}
        refreshControl={
          <RefreshControl
            onRefresh={() => this.onRefresh()}
            refreshing={this.state.isFetching}
          />
        }

        ItemSeparatorComponent={ () => ( <View style={ styles.separator}/> )}

        contentContainerStyle={styles.iterator}
      />
    );
  }
}

const styles = StyleSheet.create({
  iterator: {
    padding: 10
  },
  separator: {
    height: 10
  }
});

export default ListHomeComponent;