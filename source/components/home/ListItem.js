import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';

class ListItemHomeComponent extends React.Component {

  render() {

    const { item } = this.props;

    return (
      <View style={styles.view}>
        <Image
          style={styles.image}
          source={{ uri: `https://image.tmdb.org/t/p/w500${item.backdrop_path}` }}
        />
        <View style={styles.viewText}>
          <View style={{ flexDirection: 'row', marginBottom: 5 }} >
            <Text style={[styles.text, { fontWeight: 'bold' }]} numberOfLines={1}>
              {item.original_name}
            </Text>
            <Text style={[styles.text, { fontWeight: 'bold', textAlign: 'right' }]}>
              {item.vote_average}
            </Text>
          </View>
          <Text style={[styles.text, { textAlign: 'justify', fontSize: 12 }]} numberOfLines={4} >
            {item.overview}
          </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  view: {
    backgroundColor: "#FFFFFF",
    flexDirection: 'row',
    padding: 10,
    shadowColor: "#000000",
    shadowOffset: { width: 0, height: 1.1 },
    shadowOpacity:  0.4,
    shadowRadius: 4,
    elevation: 1
  },
  viewText: {
    marginHorizontal: 10,
    flex: 1
  },
  text: {
    color: "#333",
    flex: 1
  },
  image: {
    width: 70,
    height: 100
  }
});

export default ListItemHomeComponent;