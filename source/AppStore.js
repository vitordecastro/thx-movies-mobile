import React from 'react';
import { StatusBar } from 'react-native';
import { createStore } from 'redux';
import { Provider } from 'react-redux';

import reducers from './store/reducers';

import AppNavigation from './AppNavigation';

const store = createStore(reducers);

class AppStore extends React.Component {

  render() {
    return (
      <Provider store={store}>
        <StatusBar />
        <AppNavigation />
      </Provider>
    );
  }
}

export default AppStore;