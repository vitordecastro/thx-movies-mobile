import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import HomeScreen from './screens/HomeScreen';

const defaultNavigationOptions = {
  headerStyle: {
    backgroundColor: 'rgb(66, 139, 202)',
  },
  headerTitleStyle: {
    textAlign: 'center',
    flex: 1
  },
  headerTintColor: '#FFFFFF'
};

const AppNavigator = createStackNavigator(
  {
    HomeScreen
  },
  {
    initialRoute: 'HomeScreen',
    defaultNavigationOptions
  })

const AppContainer = createAppContainer(AppNavigator);

export default AppContainer;